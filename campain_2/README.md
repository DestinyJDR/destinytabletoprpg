![screencast](../resources/title_c2.png "Restful Peaks of Insomnia")


# Restful Peaks of Insomnia - La Seconde Campagne / The Second Campain

Ici, trouvez toutes les informations vous permettant de jouer, et faire vivre les aventures des gardiens au travers de la seconde campagne proposée par Destiny: Universe of Light : "Restful Peaks of Insomnia".<br/>
Here, find all the information you need to play, and bring the adventures of the guardians to life through the second campaign offered by Destiny: Universe of Light: "Restful Peaks of Insomnia".<br/>

Ici, vous trouverez :<br/>
Here, you will find :<br/>

Le scenario, écrit et complet, en français uniquement, du déroulement des scénario, et du fil rouge de l'histoire.<br/>
The scenario, written and complete, in French only, of how the story unfolds.<br/>

Des images à utiliser, comme des panoramas des environnements, des images des personnages et d'objets, des cartes, et bien plus encore.<br/>
Pictures to use, such as environments, images of characters and objects, maps, and more.<br/>

Des musiques, à placer dans la partie "Jukebox" de Roll20, vous permettant de lancer des sons lorsque vous le souhaiterez. Pour la partie "Soundtracks" de la campagne : https://www.destinyjdr.com/restfulpeaksofinsomnia.html.<br/>
Musics, to be placed in the "Jukebox" part of Roll20, allowing you to launch sounds when you wish. For the "Soundtracks" part of the campaign : https://www.destinyjdr.com/restfulpeaksofinsomnia.html.


# Resources

* [Website](https://www.destinyjdr.com/)
* [Twitter](https://twitter.com/DestinyJdr)
* [Contact](contact@destinyjdr.com)


# License

Ce travail, et Destiny: Universe of Light est sous licence Creative Common BY-SA : http://creativecommons.org/licenses/by-sa/2.0/fr/