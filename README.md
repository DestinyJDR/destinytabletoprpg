# Destiny: Universe of Light

Jouez en ligne avec vos amis, récupérez tout les éléments dont vous avez besoin pour tenir une partie en ligne de *Destiny: Universe of Light* !<br/>
Play online with your friends, collect all the items you need to start an online game, and play *Destiny: Universe of Light* as a game master !

![screencast](./resources/demogif.gif "A Game with the Sheets")


## Installation des Fiches / Installing the Sheets

Dans un premier temps, téléchargez dossier **roll20_template**.<br/>
In the first place, download the following folder : **roll20_template**.<br/>

**destinyjdr.css**
* Ce fichier contient tout les éléments de styles nécessaires aux fiches de personnages.
* This file contains all the style elements needed for the character sheets.

**destinyjdr.html**
* Ce fichier contient tout les champs, formulaires, boutons et éléments présents dans les fiches de personnages.
* This file contains all the fields, forms, buttons and elements found in the character sheets.

**translation.json**
* Ce fichier contient toutes les informations provenants de l'auteur.
* This file contains all the informations about the author.


Pour installer les fiches de personnages, créez une nouvelle partie sur Roll20, puis allez dans **Game Settings**.<br/>
In order to install the character sheets, create a new game, then go to **Game Settings**.

![gamesettings](./resources/gamesettings.png "Go in Game Settings")


Ensuite, dans la partie **Feuille de Personnage**, choisissez le choix **custom**.<br/>
Then, in the **Character Sheet Template** part, choose **custom**.

![custom](./resources/custom.png "Custom Choice")


Puis, copiez-collez le contenu des trois fichiers cités précédemment dans les bons champs.<br/>
Then, copy-paste the content of the three files mentioned above into the correct fields.<br/>

destinyjdr.html dans le champs **HTML** :<br/>
destinyjdr.html in the field **HTML Layout** :<br/>

![html](./resources/html.png "HTML")


destinyjdr.css dans le champs **CSS** :<br/>
destinyjdr.css in the field **CSS Styling** :

![css](./resources/css.png "CSS")


translation.json dans le champs **Translation** :<br/>
translation.json in the field **Translation** :<br/>

![translation](./resources/translation.png "translation")


Pour finir, cliquez sur **Sauvegarder** situé en bas.
To finish with, click on **Save Changes** at the bottom of the page.


## Usage

### Knowing Your Sheet

La fiche de personnage tente de reprendre la même organisation que les quatre fiches de personnages originales (disponibles au téléchargement sur le site officiel https://destinyjdr.com).<br/>
The character sheet attempts to use the same organization as the four original character sheets (available for download on the official website https://destinyjdr.com).<br/>

Elle contient 4 grandes parties, correspondant aux 4 fiches originales.<br/>
It contains 4 parts, corresponding to the 4 original character sheets.<br/>

**Gardien**
* Cette partie contient toutes les informations à propos de votre personnage.
* This part contains all the information about your character.

**Capacités**
* Cette partie contient tout les talents et les compétences de votre personnage, ainsi que d'autres éléments comme les défauts et les mutations.
* This part contains all the talents and skills of your character, as well as other elements like flaws and mutations.

**Allégeance**
* Cette partie contient toutes les informations à propos de la progression dans la Voie de l'Espoir et la Voie du Malheur de votre personnage.
* This part contains all the information about the progression in the Way of Hope and the Way of Sorrow of your character.

**Equipement**
* Cette partie contient toutes les informations à propos de l'arsenal et de l'équipement de votre personnage.
* This part contains all the information about your character's arsenal and equipment.


![parts](./resources/parts.png "Parties")


### Using Weapons

* coming soon *


## Resources

* [Website](https://www.destinyjdr.com/)
* [Twitter](https://twitter.com/DestinyJdr)
* [Contact](contact@destinyjdr.com)


## License

Ce travail, et Destiny: Universe of Light est sous licence Creative Common BY-SA : http://creativecommons.org/licenses/by-sa/2.0/fr/